#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )/"
cd $DIR"../"

IPS=`terraform output s03_workers_public_ips`

echo ""
echo " IPS Copy+Paste :)"
echo ""
echo $IPS
echo ""
echo ""
for var in "${IPS[@]}"
do
  echo "echo ssh -i cultureta-ec2k8s-private.pem ubuntu@${var}"
done

echo ssh -i cultureta-ec2k8s-private.pem ubuntu@_PUT_HERE_WORKER_IP_
