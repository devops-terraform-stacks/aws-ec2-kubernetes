#!/bin/bash

DEBIAN_FRONTEND=noninteractive

hostnamectl set-hostname "${MASTER_SERVER_NAME}"
echo "alias l='sudo tail -f /var/log/* /var/log/**/* | ccze -A'" >> /root/.bashrc
echo "alias k='kubectl'" >> /root/.bashrc
echo "alias l='sudo tail -f /var/log/* /var/log/**/* | ccze -A'" >> /home/ubuntu/.bashrc
echo "alias k='kubectl'" >> /home/ubuntu/.bashrc
echo "alias k9='/snap/k9s/current/bin/k9s'" >> /home/ubuntu/.bashrc
echo "OK FROM ${WORKER1_SERVER_NAME}:${WORKER1_PRIVATE_IP_ADDRESS}" > /NODE1
echo "OK FROM ${WORKER2_SERVER_NAME}:${WORKER1_PRIVATE_IP_ADDRESS}" > /NODE2
echo "OK FROM ${MASTER_SERVER_NAME}:${MASTER_PRIVATE_IP_ADDRESS}" > /MASTER

echo "${MASTER_PRIVATE_IP_ADDRESS}  ${MASTER_SERVER_NAME}" >> /etc/hosts
echo "${WORKER1_PRIVATE_IP_ADDRESS}  ${WORKER1_SERVER_NAME}" >> /etc/hosts
echo "${WORKER2_PRIVATE_IP_ADDRESS}  ${WORKER2_SERVER_NAME}" >> /etc/hosts
echo "export GITLAB_TOKEN=${GITLAB_API_TOKEN}" >> /etc/bash.bashrc
echo "export EDITOR=nano" >> /etc/bash.bashrc

apt update && \
apt upgrade -y && \
apt install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gpg \
    gnupg2 \
    lsb-release \
    software-properties-common \
    ccze \
    locate

cat <<EOF | tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF
modprobe overlay
modprobe br_netfilter
# sysctl params required by setup, params persist across reboots
cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-iptables  = 1
net.bridge.bridge-nf-call-ip6tables = 1
net.ipv4.ip_forward                 = 1
EOF
# Apply sysctl params without reboot
sysctl --system

wget https://github.com/containerd/containerd/releases/download/v1.7.11/containerd-1.7.11-linux-amd64.tar.gz
tar Cxzvf /usr/local containerd-1.7.11-linux-amd64.tar.gz
mkdir /etc/containerd
containerd config default > config.toml
cp config.toml /etc/containerd


wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service
cp containerd.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable --now containerd



wget https://github.com/opencontainers/runc/releases/download/v1.1.10/runc.amd64
install -m 755 runc.amd64 /usr/local/sbin/runc


wget https://github.com/containernetworking/plugins/releases/download/v1.4.0/cni-plugins-linux-amd64-v1.4.0.tgz
mkdir -p /opt/cni/bin
tar Cxzvf /opt/cni/bin cni-plugins-linux-amd64-v1.4.0.tgz
sed -i 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
systemctl restart containerd



curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.28/deb/Release.key | gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg


# This overwrites any existing configuration in /etc/apt/sources.list.d/kubernetes.list
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.28/deb/ /' | tee /etc/apt/sources.list.d/kubernetes.list


apt-get update
apt-get install -y kubelet=1.28.4-1.1 kubeadm=1.28.4-1.1 kubectl=1.28.4-1.1 nano net-tools
apt-mark hold kubelet kubeadm kubectl

swapoff -a

kubeadm init --control-plane-endpoint master:6443 --pod-network-cidr 10.0.0.0/16 > /root/kubernetes.out

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
chown $(id -u):$(id -g) $HOME/.kube/config
echo 'source <(kubectl completion bash)' >>~/.bashrc
source <(kubectl completion bash)


mkdir -p /home/ubuntu/.kube
cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
echo 'source <(kubectl completion bash)' >> /home/ubuntu/.bashrc
chown -R ubuntu:ubuntu /home/ubuntu/.kube

kubectl create -f https://raw.githubusercontent.com/projectcalico/calico/v3.27.0/manifests/tigera-operator.yaml
wget https://raw.githubusercontent.com/projectcalico/calico/v3.27.0/manifests/custom-resources.yaml
sed -i 's/192.168/10.0/' custom-resources.yaml
kubectl apply -f custom-resources.yaml

snap install k9s
curl -s https://fluxcd.io/install.sh | FLUX_VERSION=2.2.3 bash

#----------------------------------------------
# exuecute this line to open editor...
# you can set editor like this
# export EDITOR=nano or export VISUAL=nano
#----------------------------------------------
# kubectl edit configmap/coredns -n kube-system
#
# replace this
#
# forward . /etc/resolv.conf {
#     max_concurrent 1000
# }
#
# for this line, save and flux bootstrap :)
#
# forward . /etc/resolv.conf

# flux bootstrap gitlab \
#   --owner=gitlab_owner_path \
#   --token-$GITLAB_API_TOKEN \
#   --branch=main \
#   --path=clusters/main \
#   --repository=flux-bootstrap \
#   --namespace=flux-system \
#   --personal

# this contain at the end of the output
# the cli command to add worker...
# need run as root at each worker server
cat /root/kubernetes.out