locals {
  AMAZON_ROCKY_AMI     = "ami-0c1c30571d2dae5c9"
  MASTER_INSTANCE_TYPE = "t3.small"
  WORKER_INSTANCE_TYPE = "t3.micro"
  KEY_PAIR_NAME        = "${var.project_name}-KEY_PAIR"
  GITLAB_API_TOKEN     = "${var.gitlab_token}"
  HOW_MANY_WORKERS     = 3

  MASTER_PRIVATE_IP_ADDRESS  = "10.0.1.101"
  WORKERS_PRIVATE_IP_ADDRESS = tolist(["10.0.1.102","10.0.1.103","10.0.1.104"])
}

terraform {
  required_version = "~>1.7.1"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~>5.35.0"
    }

    local = {
      source  = "hashicorp/local"
      version = "2.5.1"
    }
  }


  backend "s3" {
  }
}

provider "aws" {
  profile = var.provider_profile
  region  = var.provider_region
}

resource "aws_security_group" "all-open" {
  name        = "${var.project_name}-all-open"
  description = "ingress and egress security group all open"
  vpc_id      = data.terraform_remote_state.vpc.outputs.s02_networking_vpc_id

  ingress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name        = "${var.project_name}-ssh"
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }
}

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "ssh" {
  key_name   = local.KEY_PAIR_NAME
  public_key = tls_private_key.rsa.public_key_openssh

  tags = {
    Name        = local.KEY_PAIR_NAME
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }
}

resource "local_file" "private_key_file" {
  content         = tls_private_key.rsa.private_key_pem
  filename        = "${var.project_name}-private.pem"
  file_permission = 0600
}

resource "local_file" "public_key_file" {
  content         = aws_key_pair.ssh.public_key
  filename        = "${var.project_name}-public.pub"
  file_permission = 0600
}

resource "aws_instance" "master" {
  ami                         = local.AMAZON_ROCKY_AMI
  instance_type               = local.MASTER_INSTANCE_TYPE
  key_name                    = local.KEY_PAIR_NAME
  vpc_security_group_ids      = ["${aws_security_group.all-open.id}"]
  associate_public_ip_address = true
  subnet_id                   = data.terraform_remote_state.vpc.outputs.s02_networking_public_subnets_ids[0]
  private_ip                  = local.MASTER_PRIVATE_IP_ADDRESS

  user_data = templatefile("${path.cwd}/userdata/master.sh", {
    MASTER_PRIVATE_IP_ADDRESS = local.MASTER_PRIVATE_IP_ADDRESS
    MASTER_SERVER_NAME         = "master"
    WORKER1_SERVER_NAME        = "worker1"
    WORKER2_SERVER_NAME        = "worker2"
    WORKER1_PRIVATE_IP_ADDRESS = local.WORKERS_PRIVATE_IP_ADDRESS[0]
    WORKER2_PRIVATE_IP_ADDRESS = local.WORKERS_PRIVATE_IP_ADDRESS[1]
    GITLAB_API_TOKEN           = local.GITLAB_API_TOKEN
  })

  tags = {
    Name        = "${var.project_name}-MASTER"
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }
}

resource "aws_instance" "worker" {
  count                       = local.HOW_MANY_WORKERS
  ami                         = local.AMAZON_ROCKY_AMI
  instance_type               = local.WORKER_INSTANCE_TYPE
  key_name                    = local.KEY_PAIR_NAME
  vpc_security_group_ids      = ["${aws_security_group.all-open.id}"]
  associate_public_ip_address = true
  subnet_id                   = data.terraform_remote_state.vpc.outputs.s02_networking_public_subnets_ids[0]
  private_ip                  = local.WORKERS_PRIVATE_IP_ADDRESS[count.index]

  user_data = templatefile("${path.cwd}/userdata/worker.sh", {
    MASTER_PRIVATE_IP_ADDRESS = local.MASTER_PRIVATE_IP_ADDRESS
    MASTER_SERVER_NAME         = "master"
    WORKER_SERVER_NAME         = "worker${count.index + 1}"
    WORKER1_SERVER_NAME        = "worker1"
    WORKER2_SERVER_NAME        = "worker2"
    WORKER1_PRIVATE_IP_ADDRESS = local.WORKERS_PRIVATE_IP_ADDRESS[0]
    WORKER2_PRIVATE_IP_ADDRESS = local.WORKERS_PRIVATE_IP_ADDRESS[1]
  })

  tags = {
    Name        = "${var.project_name}-WORKER_${count.index + 1}"
    Project     = var.project_name
    Environment = var.project_environment
    CreatedBy   = var.project_created_by
    Group       = var.project_group
  }
}
