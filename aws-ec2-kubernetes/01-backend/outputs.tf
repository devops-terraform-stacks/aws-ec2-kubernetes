output "s01_backend_bucket_id" {
  description = "Id of the created bucket s3"
  value       = module.backend.backend_bucket_id
}

output "s01_backend_bucket_arn" {
  description = "arn of the created bucket s3"
  value       = module.backend.backend_bucket_arn
}

output "s01_backend_bucket_name" {
  description = "name of the created bucket s3"
  value       = module.backend.backend_bucket_name
}

output "s01_backend_table_name" {
  description = "name of the storage table"
  value       = module.backend.backend_table_name
}

output "s01_debug" {
  description = "For debug purpose."
  value       = module.backend.debug
}
